from django.views.decorators.http import require_http_methods
from .models import Technician, AutomobileVO, Appointment
from service_rest.encoders import (
    TechnicianEncoder,
    AutomobileVOEncoder,
    AppointmentEncoder,
)
from django.http import JsonResponse
import json


# Create your views here.
@require_http_methods(["GET"])
def api_list_automobile_vo(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobiles": automobiles}, encoder=AutomobileVOEncoder, sold=False
        )


@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            technicians = Technician.objects.create(**content)
            return JsonResponse(technicians, encoder=TechnicianEncoder, safe=False)
        except Technician.DoesNotExist:
            response = JsonResponse(
                {"message": "Unable to create new technicians"},
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "DELETE"])
def api_technician(request, id):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=id)
            return JsonResponse(technician, encoder=TechnicianEncoder, safe=False)
        except Technician.DoesNotExist:
            response = JsonResponse(
                {"message": "Technician does not exist"},
                status=404,
            )
            return response
    else:
        try:
            technician = Technician.objects.get(employee_id=id).delete()
            return JsonResponse(
                {"confirmation": "Technician deleted"},
            )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician does not exist"},
                status=404,
            )


@require_http_methods(["GET", "POST"])
def api_appointments(request, vin=None):
    if request.method == "GET":
        if vin is None:
            appointments = Appointment.objects.all()
            return JsonResponse(
                {"appointments": appointments}, encoder=AppointmentEncoder
            )
        else:
            try:
                appointments = Appointment.objects.filter(vin=vin)
                return JsonResponse(
                    {"appointments": appointments}, encoder=AppointmentEncoder
                )
            except Appointment.DoesNotExist:
                response = JsonResponse({"message": "Appointment does not exist"})
                response.status_code = 404
                return response

    else:
        content = json.loads(request.body)
        try:
            technicians_id = content["technician"]
            tech = Technician.objects.get(id=technicians_id)
            content["technician"] = tech

            if AutomobileVO.objects.filter(vin=content["vin"]).exists():
                content["vip"] = True
            else:
                content["vip"] = False

        except Technician.DoesNotExist:
            return JsonResponse({"message": "Invalid technician id"})

        appointment = Appointment.objects.create(**content)

        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_appointment(request, id):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=id)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment does not exist"},
                status=404,
            )
    elif request.method == "PUT":
        try:
            appointment = Appointment.objects.get(id=id)
            content = json.loads(request.body)
            appointment.status = content.get("status", appointment.status)
            appointment.save()
            return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment does not exist"},
                status=404,
            )
    else:
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.delete()
            return JsonResponse(
                {"confirmation": "Appointment deleted"},
            )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Appointment does not exist"},
                status=404,
            )


@require_http_methods(["PUT"])
def api_appointment_cancel(request, id):
    appointment = Appointment.objects.get(id=id)
    appointment.status = "cancel"
    appointment.save()
    return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
            )

@require_http_methods(["PUT"])
def api_appointment_finish(request, id):
    appointment = Appointment.objects.get(id=id)
    appointment.status = "finish"
    appointment.save()
    return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
            )
