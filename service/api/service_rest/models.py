from django.db import models

# Create your models here.


class Technician(models.Model):
    employee_id = models.PositiveSmallIntegerField(unique=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)

    def __str__(self):
        return self.first_name


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin


class Appointment(models.Model):
    vin = models.CharField(max_length=17, null=True)
    status = models.CharField(max_length=10, default="scheduled")
    customer = models.CharField(max_length=100)
    date_time = models.DateTimeField()
    technician = models.ForeignKey(
        Technician, related_name="appointments", on_delete=models.CASCADE
    )
    reason = models.TextField(max_length=250)
    vip = models.BooleanField(default=False)

    def vip_check(self):
        return AutomobileVO.objects.filter(vin=self.vin, sold=True).exists()
