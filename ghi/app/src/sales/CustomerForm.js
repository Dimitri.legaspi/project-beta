import React, { useState } from 'react'


function CustomerForm() {

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [address, setAddress] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');

    const firstNameChange = (event) => {
        setFirstName(event.target.value);
    }

    const lastNameChange = (event) => {
        setLastName(event.target.value);
    }

    const addressChange = (event) => {
        setAddress(event.target.value);
    }

    const phoneNumberChange = (event) => {
        setPhoneNumber(event.target.value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.first_name = firstName;
        data.last_name = lastName;
        data.address = address;
        data.phone_number = phoneNumber;

        const customerUrl = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const customer = await fetch(customerUrl, fetchConfig);
        if (customer.ok) {
            setFirstName('');
            setLastName('');
            setAddress('');
            setPhoneNumber('');
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>Add a Customer</h1>
                <form id="create-conference-form" onSubmit={handleSubmit}>
                  <div className="form-floating mb-3">
                    <input placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" value={firstName} onChange={firstNameChange} />
                    <label htmlFor="first_name">First Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" value={lastName} onChange={lastNameChange} />
                    <label htmlFor="last_name">Last Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input placeholder="Address" required type="text" name="address" id="address" className="form-control" value={address} onChange={addressChange} />
                    <label htmlFor="address">Address</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input placeholder="Phone Number" required type="text" name="phone_number" id="phone_number" className="form-control" value={phoneNumber} onChange={phoneNumberChange} />
                    <small>Format: 123-456-7890</small>
                    <label htmlFor="phone_number">Phone Number</label>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
          </div>
    );
}

export default CustomerForm;