import React, { useState, useEffect } from "react";


function SalesList() {

    const [allSales, setAllSales] = useState([]);

    async function loadAllSales() {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok) {
            const data = await response.json();
            setAllSales(data.sales);
        }
    }

    useEffect(() => {
        loadAllSales();
    }, []);

    return (
        <>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Salesperson Employee ID</th>
                    <th>Salesperson Name</th>
                    <th>Customer</th>
                    <th>VIN</th>
                    <th>Price</th>
                </tr>
            </thead>
            <tbody>
                {allSales?.map(sales => {
                    return (
                        <tr key={sales.id}>
                            <td>{sales.sales_person.employee_id}</td>
                            <td>{sales.sales_person.first_name} {sales.sales_person.last_name}</td>
                            <td>{sales.customer.first_name} {sales.customer.last_name}</td>
                            <td>{sales.automobile.vin}</td>
                            <td>${sales.price.toFixed(2)}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </>
    );
}

export default SalesList;