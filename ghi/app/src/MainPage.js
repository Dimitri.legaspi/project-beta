import "./Main.css";
import Cards from "./cards/Cards";

function MainPage() {
  return (
    <>
      <div className="hero-container">
        <video src="/video-1.mp4" autoPlay loop muted />
        <h1>EuroMotors</h1>
        <p>
          Master the Road, European Style: EuroMotor Manager, Where Precision
          Meets Performance!
        </p>
      </div>
      <Cards />
    </>
  );
}

export default MainPage;
