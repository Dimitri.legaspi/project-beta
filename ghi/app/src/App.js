import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalespersonList from './sales/SalespersonList';
import SalespersonForm from './sales/SalespersonForm';
import CustomerList from './sales/CustomerList';
import CustomerForm from './sales/CustomerForm';
import SalesList from './sales/SalesList';
import SalesForm from './sales/SalesForm';
import SalesHistory from './sales/SalesHistory';
import AppointmentForm from './service/AppointmentForm';
import AppointmentsList from './service/AppointmentsList';
import ServiceHistory from './service/ServiceHistoryList';
import TechnicianForm from './service/TechnicianForm';
import TechniciansList from './service/TechniciansList';
import ManufacturerForm from './inventory/ManufacturerForm';
import ManufacturersList from './inventory/ManufacturersList';
import ModelForm from './inventory/ModelForm';
import ModelsList from './inventory/ModelsList';
import AutomobileForm from './inventory/AutomobileForm';
import AutomobilesList from './inventory/AutomobilesList';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="manufacturers" >
            <Route path="list" element={<ManufacturersList />} />
            <Route path="add" element={<ManufacturerForm />} />
          </Route>
          <Route path="models" >
            <Route path="list" element={<ModelsList />} />
            <Route path="add" element={<ModelForm />} />
          </Route>
          <Route path="automobiles" >
            <Route path="list" element={<AutomobilesList />} />
            <Route path="add" element={<AutomobileForm />} />
          </Route>
          <Route path="salesperson" >
            <Route path="list" element={<SalespersonList />} />
            <Route path="add" element={<SalespersonForm />} />
          </Route>
          <Route path="appointments" >
            <Route path="add" element={<AppointmentForm />} />
            <Route path="list" element={<AppointmentsList />} />
          </Route>
          <Route path="service/history/" element={<ServiceHistory />} />
          <Route path="technicians" >
            <Route path="add" element={<TechnicianForm />} />
            <Route path="list" element={<TechniciansList />} />
          </Route>
          <Route path="customer">
            <Route path="list" element={<CustomerList />} />
            <Route path="add" element={<CustomerForm />} />
          </Route>
          <Route path="sales" >
            <Route path="list" element={<SalesList />} />
            <Route path="add" element={<SalesForm />} />
          </Route>
          <Route path="sales/history/" element={<SalesHistory />}/>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
