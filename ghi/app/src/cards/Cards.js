import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import "./Cards.css";

function Cards() {
  const [models, setModels] = useState([]);

  async function loadModels() {
    const response = await fetch("http://localhost:8100/api/models/");
    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  }

  useEffect(() => {
    loadModels();
  }, []);

  return (
    <div className="container-bg">
      <div className="header">
        <h1>Check out these EPIC Cars!</h1>
      </div>
      <div className="content">
        <div className="cards__wrapper">
          <ul className="cards__items">
            {models?.map((model) => {
              return (
                <li className="cards__item" key={model.id}>
                  <Link className="cards__item__link" to="automobiles/list">
                    <figure
                      className="cards__item__pic-wrap"
                      data-category={model.name}
                    >
                      <img
                        className="cards__item__img"
                        alt="Car"
                        src={model.picture_url}
                      />
                    </figure>
                    <div className="cards__item__info">
                      <h5 className="cards__item__text">
                        {model.manufacturer.name}
                      </h5>
                    </div>
                  </Link>
                </li>
              );
            })}
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Cards;
