from django.contrib import admin
from sales_rest.models import Sale, Salesperson, Customer, AutomobileVO

# Register your models here.


admin.site.register(Sale)
admin.site.register(Salesperson)
admin.site.register(Customer)
admin.site.register(AutomobileVO)
