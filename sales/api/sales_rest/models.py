from django.db import models

# Create your models here.


class Salesperson(models.Model):
    first_name = models.CharField(max_length=250)
    last_name = models.CharField(max_length=250)
    employee_id = models.PositiveSmallIntegerField(unique=True)

    def __str__(self):
        return self.first_name


class Customer(models.Model):
    first_name = models.CharField(max_length=250)
    last_name = models.CharField(max_length=250)
    address = models.CharField(max_length=250)
    phone_number = models.CharField(max_length=15, unique=True)

    def __str__(self):
        return self.first_name


class Sale(models.Model):
    price = models.PositiveIntegerField()
    automobile = models.ForeignKey(
        "AutomobileVO", related_name="automobile", on_delete=models.CASCADE
    )
    sales_person = models.ForeignKey(
        "SalesPerson", related_name="sales_person", on_delete=models.PROTECT
    )
    customer = models.ForeignKey(
        "Customer", related_name="customer", on_delete=models.PROTECT
    )


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin
